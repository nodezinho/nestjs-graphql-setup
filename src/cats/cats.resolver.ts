import { Resolver, Args, Query, Mutation } from '@nestjs/graphql';
import { CatsService } from './cats.service';
import { CatType } from './dto/cat-type.dto';
import { Cat } from './interfaces/cat.interface';
import { CatInput } from './inputs/cat.input';

@Resolver()
export class CatsResolver {
	constructor(private readonly catsService: CatsService) {}

	@Query(() => String)
	async hello(): Promise<string> {
		return 'hello mundão de deus';
	}

	@Query(() => [CatType])
	cats(): Promise<Cat[]> {
		return this.catsService.findAll();
	}

	@Mutation(() => CatType)
	async createCat(@Args('input') input: CatInput) {
		return this.catsService.create(input);
	}
}
