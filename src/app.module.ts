import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CatsModule } from './cats/cats.module';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
	imports: [
		CatsModule,
		GraphQLModule.forRoot({
			autoSchemaFile: 'schema.gql',
		}),

		MongooseModule.forRootAsync({
			useFactory: () => ({
				uri: 'mongodb://mongodb/graphql',
				auth: {
					user: 'admin',
					password: 'admin',
				},
			}),
		}),
	],
	controllers: [AppController],
	providers: [AppService],
})
export class AppModule {}
