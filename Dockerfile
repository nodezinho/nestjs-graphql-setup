FROM node:12-alpine

COPY . ./

WORKDIR /

ARG NODE_ENV="development"
ARG APP_VERSION="development"

ENV NODE_ENV=$NODE_ENV
ENV APP_VERSION=$APP_VERSION

RUN npm install
RUN npm run build

EXPOSE 3000

CMD ["npm", "run", "start:dev"]
